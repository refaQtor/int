module resource_files;

// "-J../resources" (or wherever you've stored the files that'll get 'import()'d here) must be added to compile command for DMD

import std.file, std.stdio, std.process;


void exportExecutableFile(string file_in, string file_out)
{
    if (!std.file.exists(file_out)) {
        std.file.write(file_out, file_in);
        if (std.file.exists(file_out)) {
            string cln = "chmod 777 "; //TODO: confirm how Windows behaves
            cln ~= file_out;
            writeln(cln);
            executeShell(cln); //TODO: error handling
        }
    }
}